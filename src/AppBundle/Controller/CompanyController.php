<?php
# src/AppBundle/Controller/CompanyController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use AppBundle\Entity\Company;
use AppBundle\Form\Type\CompanyType;

class CompanyController extends Controller
{
    /**
     * @Rest\View()
     * @Rest\Get("/companies")
     */
    public function getCompagniesAction(Request $request)
    {
        $companies = $this->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:Company')
                ->findAll();

            return $companies;
    }
    
    /**
     * @Rest\View()
     * @Rest\Get("companies/{id}")
     */
    public function getCompaniesAction(Request $request)
    {
        $company = $this->get('doctrine.orm.entity_manager')
        ->getRepository('AppBundle:Company')
        ->find($request->get('id'));
        
        if(empty($company))
        {
            return \FOS\RestBundle\View\View::create(['message' => 'Company not found'], Response::HTTP_NOT_FOUND);
        }
        return $company;
    }
    
    /**
     * @Rest\View()
     * @Rest\Post("/companies")
     */
    public function postCompaniesAction(Request $request)
    {
        $company = new Company();
        $form = $this->createForm(CompanyType::class, $company);
        
        $form->submit($request->request->all());
        
        if($form->isValid()){
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($company);
            $em->flush();
            return $company;
        } else {
            return $form;
        }
        
    }
    
    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/companies/{id}")
     */
    public function removeCompaniesAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $company = $em->getRepository('AppBundle:Company')
        ->find($request->get('id'));
        if($company)
        {
            $em->remove($company);
            $em->flush();
        }
    }
    
    /**
     * @Rest\View()
     * @Rest\Patch("/companies/{id}")
     */
    public function patchCompaniesAction(Request $request)
    {
        return $this->updateCompany($request, false);
    }
    
    /**
     * @Rest\View()
     * @Rest\Put("/companies/{id}")
     */
    public function putCompaniesAction(Request $request)
    {
        return $this->updateCompany($request, true);
    }
    
    
    private function updateCompany(Request $request, $clearMissing)
    {
        $company = $this->get('doctrine.orm.entity_manager')
        ->getRepository('AppBundle:Company')
        ->find($request->get('id'));
        
        if (empty($company)) {
            return \FOS\RestBundle\View\View::create(['message' => 'Company not found'], Response::HTTP_NOT_FOUND);
        }
        
        $form = $this->createForm(CompanyType::class, $company);
        
        $form->submit($request->request->all(), $clearMissing);
        
        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($company);
            $em->flush();
            return $company;
        } else {
            return $form;
        }
    }
    
    /**
     * @Rest\View()
     * @Rest\Post("/companies/{id}/users")
     */
    public function postUserInCompany(Request $request)
    {
    }
}